<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');
$hewan = new animal("Shaun");
echo "Name : ".$hewan->name . "<br>";
echo "Legs : ".$hewan->legs . "<br>";
echo "Cold Blooded :".$hewan->coldblooded . "<br>";
echo "<br>";

echo "<br>";
$frog = new frog("Buduk");
echo "Name : ".$frog->name . "<br>";
echo "Legs : ".$frog->legs . "<br>";
echo "Cold Blooded :".$frog->coldblooded . "<br>";
echo $frog->jump();
echo "<br>";

echo "<br>";
$ape = new ape("Kera Sakti");
echo "Name : ".$ape->name . "<br>";
echo "Legs : ".$ape->legs . "<br>";
echo "Cold Blooded :".$ape->coldblooded . "<br>";
echo $ape->yell();

?>